#! /usr/bin/python
# -*- coding: utf-8 -*-
import ctypes

#VARIAVEIS
XML_NFCe_PROC = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><nfeProc xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"4.00\"><NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infNFe Id=\"NFe25190208957326000113650050000000751000000757\" versao=\"4.00\"><ide><cUF>25</cUF><cNF>00000075</cNF><natOp>VENDA DE PROD. ESTAB</natOp><mod>65</mod><serie>5</serie><nNF>75</nNF><dhEmi>2019-02-18T16:13:19-03:00</dhEmi><tpNF>1</tpNF><idDest>1</idDest><cMunFG>2504009</cMunFG><tpImp>4</tpImp><tpEmis>1</tpEmis><cDV>7</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>1</indFinal><indPres>1</indPres><procEmi>0</procEmi><verProc>Genesis PDV vr. 5.00</verProc></ide><emit><CNPJ>08957326000113</CNPJ><xNome>BLA PUB BAR EIRELI - EPP</xNome><xFant>BLA PUB CLUB</xFant><enderEmit><xLgr>R JOVINO SOBREIRA DE CARVALHO</xLgr><nro>449</nro><xBairro>JARDIM PAULISTANO</xBairro><cMun>2504009</cMun><xMun>Campina Grande</xMun><UF>PB</UF><CEP>58415305</CEP><cPais>1058</cPais><xPais>BRASIL</xPais><fone>83981055357</fone></enderEmit><IE>160658101</IE><IM>793</IM><CNAE>4665600</CNAE><CRT>3</CRT></emit><det nItem=\"1\"><prod><cProd>35</cProd><cEAN>5000000000357</cEAN><xProd>NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xProd><NCM>22030000</NCM><CEST>0302100</CEST><CFOP>5102</CFOP><uCom>UND</uCom><qCom>1.0000</qCom><vUnCom>1</vUnCom><vProd>1.00</vProd><cEANTrib>5000000000357</cEANTrib><uTrib>UND</uTrib><qTrib>1.0000</qTrib><vUnTrib>1</vUnTrib><indTot>1</indTot></prod><imposto><vTotTrib>0.10</vTotTrib><ICMS><ICMS40><orig>0</orig><CST>41</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>1.00</vBC><pPIS>1.65</pPIS><vPIS>0.02</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>1.00</vBC><pCOFINS>7.60</pCOFINS><vCOFINS>0.08</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"2\"><prod><cProd>35</cProd><cEAN>5000000000357</cEAN><xProd>CERVEJA ITAIPAVA PILSEN</xProd><NCM>22030000</NCM><CEST>0302100</CEST><CFOP>5102</CFOP><uCom>UND</uCom><qCom>1.0000</qCom><vUnCom>1</vUnCom><vProd>1.00</vProd><cEANTrib>5000000000357</cEANTrib><uTrib>UND</uTrib><qTrib>1.0000</qTrib><vUnTrib>1</vUnTrib><indTot>1</indTot></prod><imposto><vTotTrib>0.10</vTotTrib><ICMS><ICMS40><orig>0</orig><CST>41</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>1.00</vBC><pPIS>1.65</pPIS><vPIS>0.02</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>1.00</vBC><pCOFINS>7.60</pCOFINS><vCOFINS>0.08</vCOFINS></COFINSAliq></COFINS></imposto></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vFCP>0.00</vFCP><vBCST>0.00</vBCST><vST>0.00</vST><vFCPST>0.00</vFCPST><vFCPSTRet>0.00</vFCPSTRet><vProd>2.00</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vIPIDevol>0.00</vIPIDevol><vPIS>0.04</vPIS><vCOFINS>0.16</vCOFINS><vOutro>0.00</vOutro><vNF>2.00</vNF><vTotTrib>0.20</vTotTrib></ICMSTot></total><transp><modFrete>9</modFrete></transp><pag><detPag><indPag>0</indPag><tPag>01</tPag><vPag>2.00</vPag></detPag></pag><infAdic><infAdFisco>MD5:31447f41961a70fce204525302099ce6</infAdFisco><infCpl>Vendedor0: Desconhecido Operador1: ADMINISTRADOR| Consulte a NFCe em http:wwwnfefazendagovbr| email:|</infCpl></infAdic></infNFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\" /><Reference URI=\"#NFe25190208957326000113650050000000751000000757\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\" /><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" /></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\" /><DigestValue>J2w0wYiD6Yga7G/68GkGmgBvP1g=</DigestValue></Reference></SignedInfo><SignatureValue>YooNnOI2fISjWWowlKt+7wkbcNu+GVcQeopKZhUTB0ur00Y4JrSM0FFPS20r2pg1KEol2IWB/zEjdDa2NbgDUjSBKhV2HR7qLgm08/RBVwJ1iNFER74mnmvY69TSLTCOMu8VsDXAAqO2heNarB2J1YTp0CNJtICscSfe6dChJgJtXJf4FGeBXdFfVoDyPGMZceUO7liKv0HZw1lK+HSkI/aiQNTPUG13oEgRiiobfsRongs4D1MAzhX4KxjzLx6H/J5ZQqiqmebJYAJSNqzjn2YqV/B7hhiCTDQncuE1Y3DTdc1M2otVPn5bN+Nm+ARbVJVWPGLFaMFP4xSc7Xg5Aw==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIH8DCCBdigAwIBAgIIf6RTy35C6D8wDQYJKoZIhvcNAQELBQAwczELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEXMBUGA1UEAxMOQUMgU0FGRVdFQiBSRkIwHhcNMTgwNTE0MTEwNDAxWhcNMTkwNTE0MTEwNDAxWjCB4zELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxCzAJBgNVBAgTAlBCMRcwFQYDVQQHEw5DQU1QSU5BIEdSQU5ERTE2MDQGA1UECxMtU2VjcmV0YXJpYSBkYSBSZWNlaXRhIEZlZGVyYWwgZG8gQnJhc2lsIC0gUkZCMRYwFAYDVQQLEw1SRkIgZS1DTlBKIEExMRIwEAYDVQQLEwlBUiBUT1AgSUQxNTAzBgNVBAMTLEUgQkFSQk9TQSBERSBTT1VaQSBFIENJQSBMVERBOjA4OTU3MzI2MDAwMTEzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvgQTavlWVYCCtu47AnHyJLeqWpkaPECj5Y9GWY5Yj+OzX57i6zM11iYz8di81BvjDAqpNd9oXnmCYBpne12YXZ9ecsEvyo+KG8Q0W1Oj4aDA0KOMlcz7HA+wvsqI7mrUVEJeusIRapdEFxBTgd8ulC+7IYX285OgfMSZ1u2vx1nMwBixVE8Rv2z50xoOkHiJy0AKn1FJFaYx1v5W1UaqdC4qLpVrvDkrDblv7oK/tl5GEpA0Bd0eddfsKPYVjb6dUNeW5aAA5em7qN5sv/qlGgEKTD5BiOwbRUliNaboP5xxxtn4L5oXXcDpS+XMhykSF5nomU2SZNwJYJ1kpRIALQIDAQABo4IDFTCCAxEwHwYDVR0jBBgwFoAU30VPT8fh3DjMSgwg5/jpWa0fXmEwDgYDVR0PAQH/BAQDAgXgMG0GA1UdIARmMGQwYgYGYEwBAgEzMFgwVgYIKwYBBQUHAgEWSmh0dHA6Ly9yZXBvc2l0b3Jpby5hY3NhZmV3ZWIuY29tLmJyL2FjLXNhZmV3ZWJyZmIvYWMtc2FmZXdlYi1yZmItcGMtYTEucGRmMIH/BgNVHR8EgfcwgfQwT6BNoEuGSWh0dHA6Ly9yZXBvc2l0b3Jpby5hY3NhZmV3ZWIuY29tLmJyL2FjLXNhZmV3ZWJyZmIvbGNyLWFjLXNhZmV3ZWJyZmJ2Mi5jcmwwUKBOoEyGSmh0dHA6Ly9yZXBvc2l0b3JpbzIuYWNzYWZld2ViLmNvbS5ici9hYy1zYWZld2VicmZiL2xjci1hYy1zYWZld2VicmZidjIuY3JsME+gTaBLhklodHRwOi8vYWNyZXBvc2l0b3Jpby5pY3BicmFzaWwuZ292LmJyL2xjci9TQUZFV0VCL2xjci1hYy1zYWZld2VicmZidjIuY3JsMIGLBggrBgEFBQcBAQR/MH0wUQYIKwYBBQUHMAKGRWh0dHA6Ly9yZXBvc2l0b3Jpby5hY3NhZmV3ZWIuY29tLmJyL2FjLXNhZmV3ZWJyZmIvYWMtc2FmZXdlYnJmYnYyLnA3YjAoBggrBgEFBQcwAYYcaHR0cDovL29jc3AuYWNzYWZld2ViLmNvbS5icjCBtAYDVR0RBIGsMIGpgRRSSEJBUkJPU0ExQEdNQUlMLkNPTaAjBgVgTAEDAqAaExhFTUVSU09OIEJBUkJPU0EgREUgU09VWkGgGQYFYEwBAwOgEBMOMDg5NTczMjYwMDAxMTOgOAYFYEwBAwSgLxMtMjAxMDE5NTYxMTIxMDQzNTQyMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwoBcGBWBMAQMHoA4TDDAwMDAwMDAwMDAwMDAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwCQYDVR0TBAIwADANBgkqhkiG9w0BAQsFAAOCAgEAHfrnV8ihObu0jDFlkmldwWy2Mm3wr3r76EJRG1SQHf5CIDxopf0klUTaWxeLUO88E80XU92tiol6X4zEGMCM/dPYG5cW7XYbbQ6ISZDFwvaKeS+SBoxGggvhibfRpgQlbu+xCNZmSqu9bfbq58EokG6+ECISPN03OV61fJQrcuQn9bWDpJEVzyKZnDQirhFgl0yuLwH0rEARjaOigxWFeuAhy6u0qe+HowaLwt9TDt5KbTIPuYX0O+EAX+1iyhsoMRgvakGC0stRz2RS6PX/fgMqQQzVFE+6rS/dXOFspw/lMp8iAGs5oXi+dBwYjFeQ6Q/RWEBgsI0blYend6/SjciEhNnazIaLe+oYiysADIZsLm3KRkXrhtqeM3nlHUh2IJ96R9ErbFbl9sOBxK/zTJGUurNC7+sVB0kf6DnizVNhVywmvi99xBrawVorfXEEWJEYkdCDHywHkeUdU+STjgVjzmqdKL3chDdhXxoybO0LHdmWedmcN0FVfWJD0Wwl9ZTCiumezD37K/34SfV7hy+GGfyFOlnD+FXE5+1r6rbeOhR6J9hppc2bJQ4tI7w9Te7WYPu3J8CeKwiICv8BDAhPS2zkv9oiursw9+YEmJlMRNBry2sgOcdWxWHOh/WaA9S2IVhMgKtDGPdw6FlewvBiAxaQVSWk0ekSdsp1ZeM=</X509Certificate></X509Data></KeyInfo></Signature></NFe><protNFe versao=\"4.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infProt Id=\"ID325190000004680\" xmlns=\"http://www.portalfiscal.inf.br/nfe\"><tpAmb>2</tpAmb><verAplic>SVRSnfce201902111154</verAplic><chNFe>25190208957326000113650050000000751000000757</chNFe><dhRecbto>2019-02-18T16:13:27-03:00</dhRecbto><nProt>325190000004680</nProt><digVal>J2w0wYiD6Yga7G/68GkGmgBvP1g=</digVal><cStat>100</cStat><xMotivo>Autorizado o uso da NF-e</xMotivo></infProt></protNFe></nfeProc>"
XML_SAIDA_VENDA_SAT_DESENVOLVIMENTO = "<CFe><infCFe Id=\"CFe35170900246872000134590002121801051011580881\" versao=\"0.07\" versaoDadosEnt=\"0.07\" versaoSB=\"010100\"><ide><cUF>35</cUF><cNF>158088</cNF><mod>59</mod><nserieSAT>000212180</nserieSAT><nCFe>105101</nCFe><dEmi>20170919</dEmi><hEmi>201653</hEmi><cDV>1</cDV><tpAmb>1</tpAmb><CNPJ>03995946000123</CNPJ><signAC>NqtMtUKXugLtXR7knqixrLCDXLtIssXu/ZhY7hJw0ixDiJ+rpS3uM9SoF6dFQOX6DnNrIyV5+Poa3JhIz+tIL6Cl2vWiFdSTwg4Gl7qRFh3jWnbNxsVCAP33iXSNJP82aVVJNOtDlP4iBJxqa7JtS9C5gv1csqCvV5j7Pwk81uiaZHYptZPByQQbkhG6hZ+wcRdV3Fh24RfkpomyBJJqlGvD1+9kQHvJzgJ3jIRKYQyh7ZugqmVBB3BJ2ML9mSrS5fPzKtI6TgIumlBghAXw8Tbqbo3hkEqym4tbNZcNUlksAb98/jS1Rjiz1yKgGQB/VCN0JoD9YGW2i9XUjVq45Q==</signAC><assinaturaQRCODE>lAcssLXWsnzF3hxNlqrdr8SLOCrfqmONBXlKBTXEjDsb6rDsYngAiZF+3QdCNaZr7qopgmD3CbJ72unXikS9LQE+6bRYQUCN8651mZ0416pKbpwNyRxYCEcuv4JFYAYM6vasp/NikdSfB6CCTVhZWP0EAKFQwJ8p5UUWL6Z8C4+GDMG/MB4GYoolaK86YaNj4KK2ZQvzoip1CQPMQyi89qJRd1TOQrBqVTLbTVtz9h3UUheZTtUzkFcJD8LJpb+f/tSeNrsdgcDUTFP4f4vrXUekCqGkGPqoX/AfDp0zOtMWwUmk1zb6e233aupeLlMidobVR2fcx8YXKMxoXQwp8g==</assinaturaQRCODE><numeroCaixa>201</numeroCaixa></ide><emit><CNPJ>00246872000134</CNPJ><xNome>ODALTIR DE MEDEIROS e CIA LTDA</xNome><xFant>SUPERMERCADO MEDEIROS</xFant><enderEmit><xLgr>RUA SAO PAULO</xLgr><nro>1843</nro><xBairro>CENTRO</xBairro><xMun>SAO JOAQUIM DA BARRA</xMun><CEP>14600000</CEP></enderEmit><IE>642001126110</IE><IM>5317</IM><cRegTrib>3</cRegTrib><indRatISSQN>S</indRatISSQN></emit><dest/><det nItem=\"1\"><prod><cProd>1038</cProd><xProd>LARANJA PERA kg</xProd><NCM>08051000</NCM><CFOP>5102</CFOP><uCom>KG</uCom><qCom>1.4900</qCom><vUnCom>1.290</vUnCom><vProd>1.92</vProd><indRegra>A</indRegra><vItem>1.92</vItem></prod><imposto><ICMS><ICMS40><Orig>0</Orig><CST>40</CST></ICMS40></ICMS><PIS><PISNT><CST>06</CST></PISNT></PIS><COFINS><COFINSNT><CST>06</CST></COFINSNT></COFINS></imposto></det><det nItem=\"2\"><prod><cProd>7898930673134</cProd><cEAN>7898930673134</cEAN><xProd>SAKKOS 3KG 50un</xProd><NCM>39232110</NCM><CFOP>5102</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>6.950</vUnCom><vProd>6.95</vProd><indRegra>A</indRegra><vItem>6.95</vItem></prod><imposto><vItem12741>1.96</vItem12741><ICMS><ICMS00><Orig>0</Orig><CST>00</CST><pICMS>18.00</pICMS><vICMS>1.25</vICMS></ICMS00></ICMS><PIS><PISAliq><CST>01</CST><vBC>6.95</vBC><pPIS>0.0165</pPIS><vPIS>0.11</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>6.95</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.53</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"3\"><prod><cProd>7891150001954</cProd><cEAN>7891150001954</cEAN><xProd>ADES ORIG. ZERO 1L</xProd><NCM>22029900</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>7.980</vUnCom><vProd>7.98</vProd><indRegra>A</indRegra><vItem>7.98</vItem><obsFiscoDet xCampoDet=\"Cod. CEST\"><xTextoDet>0302000</xTextoDet></obsFiscoDet></prod><imposto><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>7.98</vBC><pPIS>0.0165</pPIS><vPIS>0.13</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>7.98</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.61</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"4\"><prod><cProd>7891150001954</cProd><cEAN>7891150001954</cEAN><xProd>ADES ORIG. ZERO 1L</xProd><NCM>22029900</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>7.980</vUnCom><vProd>7.98</vProd><indRegra>A</indRegra><vItem>7.98</vItem><obsFiscoDet xCampoDet=\"Cod. CEST\"><xTextoDet>0302000</xTextoDet></obsFiscoDet></prod><imposto><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>7.98</vBC><pPIS>0.0165</pPIS><vPIS>0.13</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>7.98</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.61</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"5\"><prod><cProd>7896200116114</cProd><cEAN>7896200116114</cEAN><xProd>AZ.BALTICO SUAV500ml</xProd><NCM>21039021</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>12.900</vUnCom><vProd>12.90</vProd><indRegra>A</indRegra><vItem>12.90</vItem><obsFiscoDet xCampoDet=\"Cod. CEST\"><xTextoDet>1703500</xTextoDet></obsFiscoDet></prod><imposto><vItem12741>4.05</vItem12741><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>12.90</vBC><pPIS>0.0165</pPIS><vPIS>0.21</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>12.90</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.98</vCOFINS></COFINSAliq></COFINS></imposto></det><total><ICMSTot><vICMS>1.25</vICMS><vProd>37.73</vProd><vDesc>0.00</vDesc><vPIS>0.58</vPIS><vCOFINS>2.73</vCOFINS><vPISST>0.00</vPISST><vCOFINSST>0.00</vCOFINSST><vOutro>0.00</vOutro></ICMSTot><vCFe>37.73</vCFe><vCFeLei12741>6.01</vCFeLei12741></total><pgto><MP><cMP>01</cMP><vMP>37.73</vMP></MP><vTroco>0.00</vTroco></pgto></infCFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"#CFe35170900246872000134590002121801051011580881\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>5pioY3cH5RZt9sySj8n+xTOZS8Kd/tRlTdRNrNYKbK4=</DigestValue></Reference></SignedInfo><SignatureValue>1LB2Ce7P+XuikzBxQIsUJ4xC/CmloQ9WgQyI1kMth2Vz+qUcaAlXhFIxFHWrZUi3tx1qp5n5MG4+A3nc80Mj3OuzRI9gx8lSDpBaqNu2uyDKEPFEizvjlEavCyxMFKqYO5anfmUO4O3pz3CITX0KYXHP5EHeFHShdPq9GcedtZvXkt4A7abUkt9J0BlCa/k1PGzryVPjIfNygdfyXtcgu0W8SpbL8VVYJQ/unTtjKyMhF98XH4GLHM7NyUT/5DexWNecd/uPaXISU66ioPqHvs44LYRwyFqHPI59JHiVsmTY6t5vRp3yzguxMQH90IlSQQg8aZsPLuwh1jRmAyQq3g==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIGgjCCBGqgAwIBAgIJASIsy2U/i7b1MA0GCSqGSIb3DQEBCwUAMFExNTAzBgNVBAoTLFNlY3JldGFyaWEgZGEgRmF6ZW5kYSBkbyBFc3RhZG8gZGUgU2FvIFBhdWxvMRgwFgYDVQQDEw9BQyBTQVQgU0VGQVogU1AwHhcNMTYxMTMwMTg0NTA2WhcNMjExMTMwMTg0NTA2WjCBuzESMBAGA1UEBRMJMDAwMjEyMTgwMQswCQYDVQQGEwJCUjESMBAGA1UECBMJU2FvIFBhdWxvMREwDwYDVQQKEwhTRUZBWi1TUDEPMA0GA1UECxMGQUMtU0FUMSgwJgYDVQQLEx9BdXRlbnRpY2FkbyBwb3IgQVIgU0VGQVogU1AgU0FUMTYwNAYDVQQDEy1PREFMVElSIERFIE1FREVJUk9TIGUgQ0lBIExUREE6MDAyNDY4NzIwMDAxMzQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDcOTDLK0KO8ue7soiaNEPz4UpsZB5DJklFZGyMZXqSYOxyLLsdjMBcMCTqDy/M/fGNaXu59SlyBgUQkUmaRY1kmgE5Sfp6DtV5IdjopEZ2CgbUmdA3in0o113PHjMS4GUzOgaXHiviGnU8bN2f/DmyJdYBnxk7rWsWU3xhvrkgsMFLUPNfYEhjrwGnUejD+rFiBNzE7oQkNQ4YZftfbZAkTJWRUEzpYfV7F7tfhFnQGHU+3eFMsd1E0xO0Z9VEoBvQmRvr9MGifYko2+XPn/jXnpeoxDOaTkLjpkmErfxU8KsKmzE4LAx3WNCUTpc1FgcO5rpbGXYaEzgM/3yjc5NrAgMBAAGjggHwMIIB7DAOBgNVHQ8BAf8EBAMCBeAwdQYDVR0gBG4wbDBqBgkrBgEEAYHsLQMwXTBbBggrBgEFBQcCARZPaHR0cDovL2Fjc2F0LmltcHJlbnNhb2ZpY2lhbC5jb20uYnIvcmVwb3NpdG9yaW8vZHBjL2Fjc2VmYXpzcC9kcGNfYWNzZWZhenNwLnBkZjBlBgNVHR8EXjBcMFqgWKBWhlRodHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9sY3IvYWNzYXRzZWZhenNwL2Fjc2F0c2VmYXpzcGNybC5jcmwwgZQGCCsGAQUFBwEBBIGHMIGEMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbXByZW5zYW9maWNpYWwuY29tLmJyMFIGCCsGAQUFBzAChkZodHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9jZXJ0aWZpY2Fkb3MvYWNzYXQucDdjMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAkGA1UdEwQCMAAwJAYDVR0RBB0wG6AZBgVgTAEDA6AQBA4wMDI0Njg3MjAwMDEzNDAfBgNVHSMEGDAWgBSwhYGzKM12KikkS19YSm9o2aywKjANBgkqhkiG9w0BAQsFAAOCAgEAor5yDjSp4bnQcg5q+bj2dlUCWTWjUsLI/H+9eq5Ej+mPKktPLL1DSrX9Rvu8hj3bVADK1zo/vK0irToiurWQln27OoO3GcXdNmNcdYRMIvcWYTIsU//cVYWcXtww65Rsll8yBR0/S+z/hsN+CZ2uL3u4V+VC+eOgVhmbbs+8/atOL53UMT6nDNSnkFzEt4xkiNLVfaaRU0pNrjYkMvd/L+xAHsNTrqpGy6yiRRfRUygIuyxsqM9VmtoBPizIuVcYPBF7kq9Sez3QJlo8cS5VTfy1q/7ZsIiRSr5O4r9ezlDKHIdkiqaHdDcrr9m7tij+Ix790+7OFoV/CdWwcCvjaAAJHGC1caijWMiZY6Qo4RTPECu14cr9ocGUzCfhQM3j8gSOIQoy2UrxuSLRgWPX3mjuvczukZxOy9JkalcAhTxckNjVKaULt1OVrJKbb+K8lxyuPGCYaRcWZnaDs5o7Iob/fc/fmxnzE7gr+/K40lZtUDI32bl/EKyuXWc8zvbjsyN4x1skmd6PCFhxsI8q9QkmJOs/4DlkmPgku3BX1OtlFwMmvjfR5B5io6pm6rSr9k0yhMzgjEMNaBpSkY3oMjjdUOpIUvXU2ImimYfA43PDonXBQh74Tw7+fpMC+fB5ii9dWHc+NLUDHuF9LqPE2dhNOIvreN4CAbSmOmceg3g=</X509Certificate></X509Data></KeyInfo></Signature></CFe>"
XML_SAIDA_CANCELAMENTO_SAT_DESENVOLVIMENTO = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><CFeCanc><infCFe Id=\"CFe13180314200166000166599000108160001324252883\" chCanc=\"CFe13180314200166000166599000108160001316693175\" versao=\"0.07\"><dEmi>20180305</dEmi><hEmi>142819</hEmi><ide><cUF>13</cUF><cNF>425288</cNF><mod>59</mod><nserieSAT>900010816</nserieSAT><nCFe>000132</nCFe><dEmi>20180305</dEmi><hEmi>142846</hEmi><cDV>3</cDV><CNPJ>16716114000172</CNPJ><signAC>SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT</signAC><assinaturaQRCODE>Q5DLkpdRijIRGY6YSSNsTWK1TztHL1vD0V1Jc4spo/CEUqICEb9SFy82ym8EhBRZjbh3btsZhF+sjHqEMR159i4agru9x6KsepK/q0E2e5xlU5cv3m1woYfgHyOkWDNcSdMsS6bBh2Bpq6s89yJ9Q6qh/J8YHi306ce9Tqb/drKvN2XdE5noRSS32TAWuaQEVd7u+TrvXlOQsE3fHR1D5f1saUwQLPSdIv01NF6Ny7jZwjCwv1uNDgGZONJdlTJ6p0ccqnZvuE70aHOI09elpjEO6Cd+orI7XHHrFCwhFhAcbalc+ZfO5b/+vkyAHS6CYVFCDtYR9Hi5qgdk31v23w==</assinaturaQRCODE><numeroCaixa>001</numeroCaixa></ide><emit><CNPJ>14200166000166</CNPJ><xNome>ELGIN INDUSTRIAL DA AMAZONIA LTDA</xNome><enderEmit><xLgr>AVENIDA ABIURANA</xLgr><nro>579</nro><xBairro>DIST INDUSTRIAL</xBairro><xMun>MANAUS</xMun><CEP>69075010</CEP></enderEmit><IE>111111111111</IE><IM>111111</IM></emit><dest><CPF>14808815893</CPF></dest><total><vCFe>3.00</vCFe></total><infAdic><obsFisco xCampo=\"xCampo1\"><xTexto>xTexto1</xTexto></obsFisco></infAdic></infCFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"#CFe13180314200166000166599000108160001324252883\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>pePcOYfIU+b59qGayJiJj492D9fTVhqbHEqFLDUi1Wc=</DigestValue></Reference></SignedInfo><SignatureValue>og35vHuErSOCB29ME4WRwdVPwps/mOUQJvk3nA4Oy//CVPIt0X/iGUZHMnJhQa4aS4c7dq5YUaE2yf8H9FY8xPkY9vDQW62ZzuM/6qSHeh9Ft09iP55T76h7iLY+QLl9FZL4WINmCikv/kzmCCi4+8miVwx1MnFiTNsgSMmzRnvAv1iVkhBogbAZES03iQIi7wZGzZDo7bFmWyXVdtNnjOke0WS0gTLhJbftpDT3gi0Muu8J+AfNjaziBMFQB3i1oN96EkpCKsT78o5Sb+uBux/bV3r79nrFk4MXzaFOgBoTqv1HF5RVNx2nWSoZrbpAV8zPB1icnAnfb4Qfh1oJdA==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIFzTCCBLWgAwIBAgICESswDQYJKoZIhvcNAQENBQAwaDELMAkGA1UEBhMCQlIxEjAQBgNVBAgMCVNBTyBQQVVMTzESMBAGA1UEBwwJU0FPIFBBVUxPMQ8wDQYDVQQKDAZBQ0ZVU1AxDzANBgNVBAsMBkFDRlVTUDEPMA0GA1UEAwwGQUNGVVNQMB4XDTE3MDEyNzEzMzMyMloXDTIyMDEyNjEzMzMyMlowgbIxCzAJBgNVBAYTAkJSMREwDwYDVQQIDAhBbWF6b25hczERMA8GA1UECgwIU0VGQVotU1AxGDAWBgNVBAsMD0FDIFNBVCBTRUZBWiBTUDEoMCYGA1UECwwfQXV0b3JpZGFkZSBkZSBSZWdpc3RybyBTRUZBWiBTUDE5MDcGA1UEAwwwRUxHSU4gSU5EVVNUUklBTCBEQSBBTUFaT05JQSBMVERBOjE0MjAwMTY2MDAwMTY2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtyLG8URyX8fqjOQa+rj3Rl6Z6eIX/dndhNe0rw6inNAXt06HtXQslBqnReuSanN3ssgpV6oev0ikfXA7hhmpZM7qVigTJp3+h1K9vKUlPZ5ELT36yAokpxakIyYRy5ELjP4KwFrAjQUgB6xu5X/MOoUmBKRLIiwm3wh7kUA9jZArQGD4pRknuvFuQ99ot3y6u3lI7Oa2ZqJ1P2E7NBmfdswQL8VG51by0Weivugsv3xWAHvdXZmmOrmv2W5C2U2VnsTjA3p2zQVwitZBEh6JxqLE3KljXlokbhHb1m2moSbzRLCdAJHIq/6eWL8kl2OVWViECODGoYA0Qz0wSgk/vwIDAQABo4ICNDCCAjAwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBeAwLAYJYIZIAYb4QgENBB8WHU9wZW5TU0wgR2VuZXJhdGVkIENlcnRpZmljYXRlMB0GA1UdDgQWBBTIeTKrUS19raxSgeeIHYSXclNYkDAfBgNVHSMEGDAWgBQVtOORhiQs6jNPBR4tL5O3SJfHeDATBgNVHSUEDDAKBggrBgEFBQcDAjBDBgNVHR8EPDA6MDigNqA0hjJodHRwOi8vYWNzYXQuZmF6ZW5kYS5zcC5nb3YuYnIvYWNzYXRzZWZhenNwY3JsLmNybDCBpwYIKwYBBQUHAQEEgZowgZcwNQYIKwYBBQUHMAGGKWh0dHA6Ly9vY3NwLXBpbG90LmltcHJlbnNhb2ZpY2lhbC5jb20uYnIvMF4GCCsGAQUFBzAChlJodHRwOi8vYWNzYXQtdGVzdGUuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9jZXJ0aWZpY2Fkb3MvYWNzYXQtdGVzdGUucDdjMHsGA1UdIAR0MHIwcAYJKwYBBAGB7C0DMGMwYQYIKwYBBQUHAgEWVWh0dHA6Ly9hY3NhdC5pbXByZW5zYW9maWNpYWwuY29tLmJyL3JlcG9zaXRvcmlvL2RwYy9hY3NhdHNlZmF6c3AvZHBjX2Fjc2F0c2VmYXpzcC5wZGYwJAYDVR0RBB0wG6AZBgVgTAEDA6AQDA4xNDIwMDE2NjAwMDE2NjANBgkqhkiG9w0BAQ0FAAOCAQEAAhF7TLbDABp5MH0qTDWA73xEPt20Ohw28gnqdhUsQAII2gGSLt7D+0hvtr7X8K8gDS0hfEkv34sZ+YS9nuLQ7S1LbKGRUymphUZhAfOomYvGS56RIG3NMKnjLIxAPOHiuzauX1b/OwDRmHThgPVF4s+JZYt6tQLESEWtIjKadIr4ozUXI2AcWJZL1cKc/NI7Vx7l6Ji/66f8l4Qx704evTqN+PjzZbFNFvbdCeC3H3fKhVSj/75tmK2TBnqzdc6e1hrjwqQuxNCopUSV1EJSiW/LR+t3kfSoIuQCPhaiccJdAUMIqethyyfo0ie7oQSn9IfSms8aI4lh2BYNR1mf5w==</X509Certificate></X509Data></KeyInfo></Signature></CFeCanc>"
XML_NFE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infNFe versao=\"4.00\" Id=\"NFe43190394654837000106650010000325329000325320\"><ide><cUF>43</cUF><cNF>00032532</cNF><natOp>VENDA</natOp><mod>65</mod><serie>1</serie><nNF>32532</nNF><dhEmi>2019-03-14T09:40:22-03:00</dhEmi><tpNF>1</tpNF><idDest>1</idDest><cMunFG>4302105</cMunFG><tpImp>4</tpImp><tpEmis>9</tpEmis><cDV>0</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>1</indFinal><indPres>1</indPres><procEmi>0</procEmi><verProc>ACBrNFe</verProc><dhCont>2019-03-14T09:40:22-03:00</dhCont><xJust>Rejeicao: Operacao com ICMS-ST sem informacao do CEST</xJust></ide><emit><CNPJ>94654837000106</CNPJ><xNome>MERCADO DE ALIMENTOS LUGAMA LTDA EPP TESTE</xNome><xFant>GETWAY AUTOMACAO</xFant><enderEmit><xLgr>AV TESTES TESTE ,S/N-QD 9-LT 9 9 9 999</xLgr><nro>S/N</nro><xBairro>GUANABARA</xBairro><cMun>4302105</cMun><xMun>BENTO GONCALVES</xMun><UF>RS</UF><CEP>13073300</CEP><cPais>1058</cPais><xPais>BRASIL</xPais><fone>1937948200</fone></enderEmit><IE>0100071244</IE><CRT>3</CRT></emit><autXML><CNPJ>07437992000168</CNPJ></autXML><det nItem=\"1\"><prod><cProd>119944</cProd><cEAN>7898302000346</cEAN><xProd>NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xProd><NCM>34022000</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>0.9900000000</vUnCom><vProd>0.99</vProd><cEANTrib>7898302000346</cEANTrib><uTrib>UN</uTrib><qTrib>1.0000</qTrib><vUnTrib>0.9900000000</vUnTrib><indTot>1</indTot></prod><imposto><vTotTrib>0.33</vTotTrib><ICMS><ICMS60><orig>0</orig><CST>60</CST></ICMS60></ICMS><PIS><PISAliq><CST>01</CST><vBC>0.99</vBC><pPIS>1.6500</pPIS><vPIS>0.02</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>0.99</vBC><pCOFINS>7.6000</pCOFINS><vCOFINS>0.08</vCOFINS></COFINSAliq></COFINS></imposto></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vFCP>0.00</vFCP><vBCST>0.00</vBCST><vST>0.00</vST><vFCPST>0.00</vFCPST><vFCPSTRet>0.00</vFCPSTRet><vProd>0.99</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vIPIDevol>0.00</vIPIDevol><vPIS>0.02</vPIS><vCOFINS>0.08</vCOFINS><vOutro>0.00</vOutro><vNF>0.99</vNF><vTotTrib>0.33</vTotTrib></ICMSTot></total><transp><modFrete>9</modFrete></transp><pag><detPag><tPag>01</tPag><vPag>0.99</vPag></detPag></pag><infAdic><infCpl>Trib aprox R$ 0,16 Federal, R$ 0,17 Estadual;Fonte: IBPT/FECOMERCIO (UF) ca7gi3;PARAIBA LEGAL - RECEITA CIDADA;TORPEDO PREMIADO: ;0100071244 14032019 032532 099;Operador: 1-Admin PDVCFE 2.2.1.0;Voce deixou de ganhar 0 NETPOINTS...;www.netpoints.com.br;________________________________________________;Powered by Getway www.getwayautomacao.com.br</infCpl></infAdic></infNFe><infNFeSupl><qrCode><![CDATA[https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx?p=43190394654837000106650010000325329000325320|2|2|14|0.99|6E77755958656A594A4F62794E48756979613864306435397A4A633D|1|AF1C74B45CAD9BA6F41762FF75FE275B43F56478]]></qrCode><urlChave>http://www.sefaz.rs.gov.br/nfce/consulta</urlChave></infNFeSupl><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"#NFe43190394654837000106650010000325329000325320\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>nwuYXejYJObyNHuiya8d0d59zJc=</DigestValue></Reference></SignedInfo><SignatureValue>eIF3dor0x2Bj2/wDq6Rd7m5cE4owHYdt/3AlYrmtg8EL5mSkxC2x74KoUDAlOmSHznfhBY7+IYydqx4jFPRQjEaRXScwfBnxT9gJ41LF8GpjVhqhCk5E2HYEHj4gWw0ZjTrk3LUf3v35F0eTiX7OK1Z0iDdilIa9UHfnO+m7/TmNXwThHOemlhpheePU00agMHTHMZw9QWHdjy3ArkD3dayzzzCaxx//eDpYpBo4O/FM5t2dCP5AsGkuxLzycdMPrs5R6FqP+MmZ852dMLBP3BLFlxO7fP30MRjTe700gYmHIOjwVJXJWefqz0/cQJuVC2iW+giVIUP9oCTTtj6WAA==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIHxDCCBaygAwIBAgIIJf8YEBFBou4wDQYJKoZIhvcNAQELBQAwgYkxCzAJBgNVBAYTAkJSMRMwEQYDVQQKEwpJQ1AtQnJhc2lsMTQwMgYDVQQLEytBdXRvcmlkYWRlIENlcnRpZmljYWRvcmEgUmFpeiBCcmFzaWxlaXJhIHYyMRIwEAYDVQQLEwlBQyBTT0xVVEkxGzAZBgNVBAMTEkFDIFNPTFVUSSBNdWx0aXBsYTAeFw0xODEwMTUxNjUxMDdaFw0xOTEwMTUxMjQ3MDBaMIHgMQswCQYDVQQGEwJCUjETMBEGA1UEChMKSUNQLUJyYXNpbDE0MDIGA1UECxMrQXV0b3JpZGFkZSBDZXJ0aWZpY2Fkb3JhIFJhaXogQnJhc2lsZWlyYSB2MjESMBAGA1UECxMJQUMgU09MVVRJMRswGQYDVQQLExJBQyBTT0xVVEkgTXVsdGlwbGExGjAYBgNVBAsTEUNlcnRpZmljYWRvIFBKIEExMTkwNwYDVQQDEzBMVUdBTUEgQ09NRVJDSU8gREUgQUxJTUVOVE9TIExUREE6OTQ2NTQ4MzcwMDAxMDYwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCrEVjEeeMJJHWogVkhUr3U9/yJnsL1Z8gJLrgJNY9SCX2bcmZA5RJ5LtXdPnbK7F2WPhko6l0sgnZ9vKS8fhvobpvMd9b6T5cUQZ6sCzZzalN3Izb81uL90yVoGcb/9X3Ny+KYFz+aaeIWIku6cVy95mB2f/WyadsGyuMqIT0T7jlh5zoo8yvjNm4lQZ8AK4bVC8XLRLyAe0DBGlpCZwe+vAzMjmiVl99NJLYD4eWq2Owq8kTkKOlCTNt1eWouP1hIy1LrvDVtFGMCi5oVc6FLwgIkn0QVTajnzbapInXSHSTqrRCeasoflSjUKV/UGPYTllY1jKqKY2YxMthsxll3AgMBAAGjggLVMIIC0TBUBggrBgEFBQcBAQRIMEYwRAYIKwYBBQUHMAKGOGh0dHA6Ly9jY2QuYWNzb2x1dGkuY29tLmJyL2xjci9hYy1zb2x1dGktbXVsdGlwbGEtdjEucDdiMB0GA1UdDgQWBBRZr2jaXnpkgpP73mvQS5vsfQ2VaTAJBgNVHRMEAjAAMB8GA1UdIwQYMBaAFDWuMRT2XtJ6T1j+NKgaZ5cKxJsHMF4GA1UdIARXMFUwUwYGYEwBAgEmMEkwRwYIKwYBBQUHAgEWO2h0dHBzOi8vY2NkLmFjc29sdXRpLmNvbS5ici9kb2NzL2RwYy1hYy1zb2x1dGktbXVsdGlwbGEucGRmMIHeBgNVHR8EgdYwgdMwPqA8oDqGOGh0dHA6Ly9jY2QuYWNzb2x1dGkuY29tLmJyL2xjci9hYy1zb2x1dGktbXVsdGlwbGEtdjEuY3JsMD+gPaA7hjlodHRwOi8vY2NkMi5hY3NvbHV0aS5jb20uYnIvbGNyL2FjLXNvbHV0aS1tdWx0aXBsYS12MS5jcmwwUKBOoEyGSmh0dHA6Ly9yZXBvc2l0b3Jpby5pY3BicmFzaWwuZ292LmJyL2xjci9BQ1NPTFVUSS9hYy1zb2x1dGktbXVsdGlwbGEtdjEuY3JsMA4GA1UdDwEB/wQEAwIF4DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwgb0GA1UdEQSBtTCBsoEebXVsdGlsdWdhbWFAbXVsdGlsdWdhbWEuY29tLmJyoCIGBWBMAQMCoBkTF01BUkNFTE8gRkVSUkVJUkEgVE9HTk9OoBkGBWBMAQMDoBATDjk0NjU0ODM3MDAwMTA2oDgGBWBMAQMEoC8TLTE3MDUxOTc4OTM0MDg0NDcwMDQxMjY4Mjk3NTY4NDAwMDAwMDAwMDAwMDAwMKAXBgVgTAEDB6AOEwwwMDAwMDAwMDAwMDAwDQYJKoZIhvcNAQELBQADggIBAIzdb9HJZi1cNccscKhV12U4mMHNUbsnDafpnrQAUigFMJsLuXtU9qTR6DenhhznM88fXghxnFICjq+/B0pTQqlDHybcJxz7nVxBzo9Ap9ix1Cq94Wps0fsqGn5bkIYH1bP27HFa9LpQ0O7V/e33t5UFKhqjkEFjSUzbg2948zU1teaGaBny+AkUIoiebAy95mkp0rvda1LDk46ksOVOV49FgvnbsN73AYzvd/EDswrhL1kyS6IegQAEThxMb3m2SJWYWUQ+Ak2u31+VGWOi0vHmfHGw7u2nr1ewQUA8IBV/vQhpUqEqJ/sW43Iihewxmg7VjNAyQVLZCYjDa/LvKeLyxAASEK3Ts7aRazheJB/lJYa3OUvAZanAiiU3V7D0W32FlG4rMrYUp/UEO2K7zYmVN3Xj+h1MADazAOGD0IIa3GEVLR95+zYKQrvWFOrRUUFP6UPT1nwwsIlcg64f1g7Dt7tYSCVL9nKdXcW5gk50cn+kzovsNm6LXr07NJ2tsh+s1xrMlgp2ClD5QbeEs/CxIn1DvSqG3Xh7SmdWAVZRWtcTpgvG3sH/tILhj6uR7Hkrjiy3Gyj5uS1ezNQEI8JTzbE3N/63rk8bhowgqbcdx3cM6Tg8NrnfQjLcc7vxUFNfZVOPoVukBXa4k+efe9i0cBog3j5f5aHDLOB3wiOW</X509Certificate></X509Data></KeyInfo></Signature></NFe>"


texto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
caracteres = "1234567890 \nABCÇDEFGHIJKLMNOPQRSTUVWXYZ \nabcçdefghijklmnopqrstuvwxyz \n!@#$%¨&*()_+'1234567890-=¹²³£¢¬7890-§,.;/<>:?°~]^}º´[`{ª \n\"/(á|à|ã|â|ä)/\",\"/(Á|À|Ã|Â|Ä)/\",\"/(é|è|ê|ë)/\",\"/(É|È|Ê|Ë)/\",\"/(í|ì|î|ï)/\",\"/(Í|Ì|Î|Ï)/\",\"/(ó|ò|õ|ô|ö)/\",\"/(Ó|Ò|Õ|Ô|Ö)/\",\"/(ú|ù|û|ü)/\",\"/(Ú|Ù|Û|Ü)/\",\"/(ñ)/\",\"/(Ñ)/\""

upcA = "01234567890"
upcE = "02345678912"
jan13 = "001234567890"
jan8 = "01234567"
code39 = "*0123456789*"
testeCode = "*TESTECODE93*"
itf = "1234567890"
codeBar = "A1234+5678B"
retb = "{C35170900246872000134590002121801051011580881"
    

#CARREGA BIBLIOTECA
_lib = ctypes.CDLL("libE1_Impressora.so.1.0.0")

#Abre conexão com a impressora I9 na porta usb
retorno = _lib.AbreConexaoImpressora(1, "I9", "USB", 0)
print("Retorno ACI: ", retorno)

#Imprime cupom NFCe em contingencia
retorno = _lib.ImprimeXMLNFCe(XML_NFE, 2, "0A51FFF2-489B-DAAB-6B3E-8034A2C6949C")
print("Retorno IXN", retorno)

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)

#Imprime Danfe NFCe processada
retorno = _lib.ImprimeXMLNFCe(XML_NFCe_PROC, 2, "0A51FFF2-489B-DAAB-6B3E-8034A2C6949C")
print("Retorno IXN", retorno)

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)

#Imprime Danfe SAT
retorno = _lib.ImprimeXMLSAT(XML_SAIDA_VENDA_SAT_DESENVOLVIMENTO)
print("Retorno IXS", retorno)

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)

#Imprime Danfe de cancelamento SAT
retorno = _lib.ImprimeXMLCancelamentoSAT(XML_SAIDA_CANCELAMENTO_SAT_DESENVOLVIMENTO, "blcamrPugGfK1VV8ixHUyfgh66vhzH7hVURbW1BaHR4eBHHtWBaZmcUuxSdpkaSl8WZ9umJkhJ4JXfKCAxJrJSRqQ6ZPpdFvEEnInJGbZ71mnM0ofTsN4+kJwem8Ky/nagqHSrZnVP3mr3L4j8c5eaiarSqu4tjgXSNnbhfntwhB8RNTxx1irsTZeJmqFOeQJ+XSkkW+ScWbGMw+gRlsI2ZTJgxaaTha1v6cltDscdeKuCYfVgu5U+WqffbAZrb9mw71rY4ccARpHo8c7qn+dEknS5f5LSfnm7llosFhlOwK+wvFKV47PRvrIHZStUxPZSw2F5FPq+IRtfvrB2EAuA==")
print("Retorno IXCS", retorno)

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)



#Exemplos de impressao de texto===============================
_lib.ImpressaoTexto("Fonte Normal", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 0, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto("Fonte Normal Sublinhado ", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 2, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto("Fonte Normal Modo Reverso", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 4, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto("Fonte Normal Negrito", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 8, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto("Mini Fonte", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 1, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto("Mini Fonte Reverso", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 5, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto("Mini Fonte Negrito", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 0, 9, 0)
_lib.AvancaPapel(2)

_lib.ImpressaoTexto("Centralizado ", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 0, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 2, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 4, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 8, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 1, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 5, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 1, 9, 0)
_lib.AvancaPapel(2)

_lib.ImpressaoTexto("Alinhado a Direita", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 0, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 2, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 4, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 8, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 1, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 5, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(caracteres, 2, 9, 0)
_lib.AvancaPapel(2)

_lib.ImpressaoTexto("Variações de Tamanho", 1, 10, 0)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 1)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 2)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 3)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 4)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 5)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 6)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 7)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 16)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 32 )
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 48)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 64)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 80)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 96)
_lib.AvancaPapel(2)
_lib.ImpressaoTexto(texto, 1, 0, 112)
#Fim exemplos de impressao de texto===========================

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)

#Teste de codigo de barras====================================
_lib.ImpressaoTexto("UPC-A", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(0, upcA, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("UPC-E", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(1, upcE, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("JAN 13 / EAN 13", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(2, jan13, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("CODE 39", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(4, code39, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("CODE 93", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(7, testeCode, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("ITF", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(5, itf, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("CODEBAR", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(6, codeBar, 50, 1, 1);
_lib.AvancaPapel(2);

_lib.ImpressaoTexto("CODE 128", 1, 10, 0);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 50 - Largura 1 - HRI Acima do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(8, retb, 50, 1, 1);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 60 - Largura 2 - HRI Abaixo do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(8,retb, 60, 2, 2);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 70 - Largura 2 - HRI Ambos", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(8,retb, 70, 2, 3);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 80 - Largura 2- HRI Acima do Código ", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(8,retb, 80, 2, 1);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 90 - Largura 2 - HRI Abaixo do Código", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(8, retb, 90, 2, 2);
_lib.AvancaPapel(2);
_lib.ImpressaoTexto("Altura 100 - Largura 2- HRI Ambos", 1, 10, 0);
_lib.AvancaPapel(1);
_lib.ImpressaoCodigoBarras(8, retb, 100, 2, 3);
#Fim exemplos de codigo de barras=============================

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)

#Exemplos de QRCode===========================================
_lib.ImpressaoQRCode("http://www4.fazenda.rj.gov.br/consultaNFCe/QRCode?p=33180705481336000137651000000916401000005909|2|2|03|10.00|376b4352314d2f646d2f44514d30576d517958534c6a37723650343d|1|122BB99337074E241AB621D6FF63F24146BD73450", 4, 2);
#Fim exemplos de codigo de barras=============================

#Corta papel com avanço de 5 linhas
retorno = _lib.Corte(5)
print("Retorno C", retorno)


#Fecha conexao com a impressora
retorno = _lib.FechaConexaoImpressora()
print("Retorno FC", retorno)
